import React, { Component } from "react";
import {
  Col,
  Container,
  Row
} from "reactstrap";

//import icon
import FeatherIcon from "feather-icons-react";

import { Link } from "react-router-dom";

// feature Image
import Img1 from "../assets/images/project/img-1.jpg";
import Img2 from "../assets/images/project/img-2.jpg";
import Img3 from "../assets/images/project/img-3.jpg";
import Img4 from "../assets/images/project/img-4.jpg";
import Img5 from "../assets/images/project/img-5.jpg";
import Img6 from "../assets/images/project/img-6.jpg";
import '../assets/css/pricing.css'
export default class Project extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: [
        {
          id: 1,
          img: Img1,
          title: 'Professional Designer',
          designation: 'UI/UX Designer'
        },
        {
          id: 2,
          img: Img2,
          title: 'Product Designer',
          designation: 'Web Designer'
        },
        {
          id: 3,
          img: Img3,
          title: 'Design Development',
          designation: 'PHP Developer'
        },
        {
          id: 4,
          img: Img4,
          title: 'Product Designer',
          designation: 'React Developer'
        },
        {
          id: 5,
          img: Img5,
          title: 'Design Development',
          designation: 'UI/UX Designer'
        },
        {
          id: 6,
          img: Img6,
          title: 'Graphic Development',
          designation: 'Web Designer'
        },
      ],
    };
  }

  render() {
    return (
      <React.Fragment>
        <section className="section bg-light" id="project">
          <Row className="justify-content-center">
            <div className="col-lg-7">
              <div className="text-center mb-5">
                <h2 className="">Pricing</h2>
                <p className="text-muted">We offer the best prices you can ever get.</p>
              </div>
            </div>
          </Row>
          <Container>

            <Row>
              {/* Render Footer Link */}
              {/* {this.state.projects.map((item, key) => (
                <Col lg={4} md={6} key={key}>
                  <div className="project-box mb-4">
                    <div className="position-relative overflow-hidden rounded">
                      <img src={item.img} alt="" className="img-fluid d-block mx-auto shadow" />
                      <div className="project-overlay">
                        <div className="project-content">
                          <Link to="#">
                            <div className="project-icon">
                              <i>
                                <FeatherIcon icon="eye" />
                              </i>
                            </div>
                          </Link>
                        </div>
                      </div>
                    </div>
                    <div className="p-3">
                      <h4 className="f-17 mb-1">
                        <Link to="#" className="text-dark">{item.title}</Link>
                      </h4>
                      <p className="text-muted">{item.designation}</p>
                    </div>
                  </div>
                </Col>
              ))} */}

              <Col lg={12} md={12} >
                {/* <div className="project-box mb-4">
                  <div class="container">
                    <div class="row">
                      <div class="col-4">
                        <div class="card text-center">
                          <div class="card-header text-center border-bottom-0 bg-transparent text-success pt-4">
                            <h5>Pay as You Go</h5>
                          </div>
                          <div class="card-body">
                            <h1>$299</h1>
                            <h5 class="text-muted"><small>Taxes per Month</small></h5>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item"><i class="fas fa-male text-success mx-2"></i>Real-time fee reporting</li>
                            <li class="list-group-item"><i class="fas fa-venus text-success mx-2"></i>Pay only for what you use</li>
                            <li class="list-group-item"><i class="fas fa-gavel text-success mx-2"></i> No setup, monthly, or hidden fees</li>
                          </ul>
                          <div class="card-footer border-top-0">
                            <a href="#" class="text-muted text-uppercase">Create Account <i class="fas fa-arrow-right"></i></a>
                          </div>
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="card text-center">
                          <div class="card-header text-center border-bottom-0 bg-transparent text-success pt-4">
                            <h5>Pay as You Go</h5>
                          </div>
                          <div class="card-body">
                            <h1>$299</h1>
                            <h5 class="text-muted"><small>Taxes per Month</small></h5>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item"><i class="fas fa-male text-success mx-2"></i>Real-time fee reporting</li>
                            <li class="list-group-item"><i class="fas fa-venus text-success mx-2"></i>Pay only for what you use</li>
                            <li class="list-group-item"><i class="fas fa-gavel text-success mx-2"></i> No setup, monthly, or hidden fees</li>
                          </ul>
                          <div class="card-footer border-top-0">
                            <a href="#" class="text-muted text-uppercase">Create Account <i class="fas fa-arrow-right"></i></a>
                          </div>
                        </div>
                      </div>
                      <div class="col-4">
                        <div class="card text-center">
                          <div class="card-header text-center border-bottom-0 bg-transparent text-success pt-4">
                            <h5>Pay as You Go</h5>
                          </div>
                          <div class="card-body">
                            <h1>$299</h1>
                            <h5 class="text-muted"><small>Taxes per Month</small></h5>
                          </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item"><i class="fas fa-male text-success mx-2"></i>Real-time fee reporting</li>
                            <li class="list-group-item"><i class="fas fa-venus text-success mx-2"></i>Pay only for what you use</li>
                            <li class="list-group-item"><i class="fas fa-gavel text-success mx-2"></i> No setup, monthly, or hidden fees</li>
                          </ul>
                          <div class="card-footer border-top-0">
                            <a href="#" class="text-muted text-uppercase">Create Account <i class="fas fa-arrow-right"></i></a>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div> */}

                <div class="container">
                  <div class="row">
                    <div class="col-3">
                      <div class="card">
                        <div class="hover-effect"></div>
                        <div class="type">PERSONAL</div>
                        <div class="price">
                          <span class="price-currency">$</span>
                          <span class="price-value">15</span>
                          <span class="per-month">/MO</span>
                        </div>
                        <ul class="chars">
                          <li><span class="num">3</span> WEBSITES</li>
                          <li><span class="num">20 GB</span> SSD</li>
                          <li><span class="num">1</span> DOMAIN NAME</li>
                          <li><span class="num">5</span> EMAIL</li>
                          <li><span class="num">1X</span> CPU & RAM</li>
                        </ul>
                        <button class="order-btn">ORDER NOW</button>
                      </div>
                    </div>
                    <div class="col-3">
                      <div class="card">
                        <div class="hover-effect"></div>
                        <div class="type">PERSONAL</div>
                        <div class="price">
                          <span class="price-currency">$</span>
                          <span class="price-value">15</span>
                          <span class="per-month">/MO</span>
                        </div>
                        <ul class="chars">
                          <li><span class="num">3</span> WEBSITES</li>
                          <li><span class="num">20 GB</span> SSD</li>
                          <li><span class="num">1</span> DOMAIN NAME</li>
                          <li><span class="num">5</span> EMAIL</li>
                          <li><span class="num">1X</span> CPU & RAM</li>
                        </ul>
                        <button class="order-btn">ORDER NOW</button>
                      </div>
                    </div>
                    <div class="col-3">
                      <div class="card">
                        <div class="hover-effect"></div>
                        <div class="type">PERSONAL</div>
                        <div class="price">
                          <span class="price-currency">$</span>
                          <span class="price-value">15</span>
                          <span class="per-month">/MO</span>
                        </div>
                        <ul class="chars">
                          <li><span class="num">3</span> WEBSITES</li>
                          <li><span class="num">20 GB</span> SSD</li>
                          <li><span class="num">1</span> DOMAIN NAME</li>
                          <li><span class="num">5</span> EMAIL</li>
                          <li><span class="num">1X</span> CPU & RAM</li>
                        </ul>
                        <button class="order-btn">ORDER NOW</button>
                      </div>
                    </div>
                    <div class="col-3">
                      <div class="card">
                        <div class="hover-effect"></div>
                        <div class="type">PERSONAL</div>
                        <div class="price">
                          <span class="price-currency">$</span>
                          <span class="price-value">15</span>
                          <span class="per-month">/MO</span>
                        </div>
                        <ul class="chars">
                          <li><span class="num">3</span> WEBSITES</li>
                          <li><span class="num">20 GB</span> SSD</li>
                          <li><span class="num">1</span> DOMAIN NAME</li>
                          <li><span class="num">5</span> EMAIL</li>
                          <li><span class="num">1X</span> CPU & RAM</li>
                        </ul>
                        <button class="order-btn">ORDER NOW</button>
                      </div>
                    </div>

                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </section>
      </React.Fragment>
    );
  }
}
